﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FastWindowSwitch
{
    public class AutocompleteCombo : ComboBox
    {
        private bool _queryNeeded;
        private bool _querying;
        private string _lastQuery;

        public override int SelectedIndex => Items.Count > 0 ? base.SelectedIndex : -1;

        public IReadOnlyCollection<object> UnfilteredItems { get; set; } = new object[0];

        protected override void OnTextUpdate(EventArgs e)
        {
            base.OnTextUpdate(e);
            _queryNeeded = !_querying;
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
            _queryNeeded = false;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            if(_queryNeeded)
            {
                DoQuery(Text);
            }
        }

        private void DoQuery(string text)
        {
            _queryNeeded = false;
            if (text == _lastQuery)
            {
                return;
            }
            _lastQuery = text;

            try
            {
                _querying = true;
                DoQueryIntern(text);
                _lastQuery = text;
            }
            finally
            {
                _querying = false;
            }
        }

        private void DoQueryIntern(string text)
        {
            var selStart = SelectionStart;

            var items = UnfilteredItems
                .Where(x => (x?.ToString() ?? "").IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                .ToList();

            DataSource = items;
            DroppedDown = true;

            Text = text;
            SelectionStart = selStart;
        }
    }
}
