﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FastWindowSwitch
{
    public class SwitchableWindow
    {
        private readonly IntPtr _handle;

        public string Name { get; }

        public SwitchableWindow(IntPtr handle)
        {
            _handle = handle;

            var sb = new StringBuilder(256);
            var len = NativeMethods.GetWindowText(handle, sb, sb.Capacity);
            if (len > 0)
            {
                Name = sb.ToString();

                NativeMethods.GetWindowThreadProcessId(handle, out var pid);
                using (var process = Process.GetProcessById(pid))
                {
                    Name += $" [{process.ProcessName}]";
                }
            }
        }

        public void Focus()
        {
            if (NativeMethods.IsIconic(_handle))
            {
                NativeMethods.ShowWindow(_handle, NativeMethods.CmdRestore);
            }
            NativeMethods.SetForegroundWindow(_handle);
        }

        public static List<SwitchableWindow> GetAll()
        {
            var retVal = new List<SwitchableWindow>();
            NativeMethods.EnumDesktopWindows(IntPtr.Zero, (hwnd, lparam) =>
            {
                if (IsWindowVisible(hwnd))
                {
                    var window = new SwitchableWindow(hwnd);
                    if (!string.IsNullOrEmpty(window.Name))
                    {
                        retVal.Add(window);
                    }
                }
                return true;
            }, IntPtr.Zero);
            return retVal.OrderBy(x => x.Name).ToList();
        }

        private static bool IsWindowVisible(IntPtr hwnd)
        {
            var style = NativeMethods.GetWindowLong(hwnd, NativeMethods.GwlStyle);
            var visible = (style & (NativeMethods.WsVisible | NativeMethods.WsMinimize)) != 0;
            return visible;
        }

        public override string ToString() => Name ?? "<null>";
    }
}
