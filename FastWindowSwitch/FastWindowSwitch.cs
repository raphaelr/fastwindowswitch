﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace FastWindowSwitch
{
    public sealed partial class FastWindowSwitch : Form
    {
        public FastWindowSwitch()
        {
            InitializeComponent();
            CreateHandle();
        }

        public void ShowWindowSwitcher(bool atMouseCursor)
        {
            Show();
            NativeMethods.POINT cursor;
            Point topLeft;
            if (atMouseCursor && NativeMethods.GetCursorPos(out cursor))
            {
                topLeft = new Point(cursor.X, cursor.Y);
            }
            else
            {
                var bounds = Screen.PrimaryScreen.Bounds;
                var cx = bounds.X + bounds.Width / 2;
                var cy = bounds.Y + bounds.Height / 2;
                topLeft = new Point(cx, cy);
            }
            Location = new Point(topLeft.X - 10, topLeft.Y - 10);
            Reload();
            Activate();

            comboWindows.Focus();
        }


        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            NativeMethods.RegisterHotKey(Handle, 1,
                NativeMethods.ModAlt | NativeMethods.ModNoRepeat,
                (uint)Keys.PageUp);
        }

        protected override void WndProc(ref Message msg)
        {
            if (msg.Msg == 0x0312 && msg.WParam == new IntPtr(1))
            {
                if (!Visible)
                {
                    ShowWindowSwitcher(true);
                }
            }
            else
            {
                base.WndProc(ref msg);
            }
        }

        private void Reload()
        {
            comboWindows.Text = "Loading...";

            var items = new List<object>();
            items.Add("");
            items.AddRange(SwitchableWindow.GetAll());

            comboWindows.DataSource = comboWindows.UnfilteredItems = items;
            comboWindows.SelectedIndex = 0;
        }

        private void comboWindows_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Hide();
            }
            if (e.KeyCode == Keys.Down)
            {
                comboWindows.DroppedDown = true;
            }
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
            Hide();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            NativeMethods.UnregisterHotKey(Handle, 1);
        }

        private void comboWindows_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboWindows.SelectedItem is SwitchableWindow window)
            {
                window.Focus();
                Hide();
            }
        }
    }
}
