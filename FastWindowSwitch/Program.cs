﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FastWindowSwitch
{
    public class Program : ApplicationContext
    {
        private readonly NotifyIcon _notifyIcon;
        private readonly Icon _icon;
        private readonly FastWindowSwitch _switcher;

        public Program()
        {
            var bmp = new Bitmap(64, 64);
            using (var g = Graphics.FromImage(bmp))
            using (var font = new Font(FontFamily.GenericSansSerif, 16))
                g.DrawString("FWS", font, Brushes.White, 0, 0);
            _icon = Icon.FromHandle(bmp.GetHicon());

            _notifyIcon = new NotifyIcon
            {
                Icon = _icon,
                Visible = true,
                ContextMenu = new ContextMenu
                {
                    MenuItems =
                    {
                        new MenuItem("Exit", (s, e) => ExitThread())
                    }
                }
            };
            _notifyIcon.Click += OnClick;

            _switcher = new FastWindowSwitch();
        }

        private void OnClick(object sender, EventArgs e)
        {
            _switcher.ShowWindowSwitcher(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _notifyIcon.Visible = false;
                _notifyIcon.Dispose();
                _icon.Dispose();
            }
            base.Dispose(disposing);
        }

        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            var app = new Program();
            Application.Run(app);
        }
    }
}
