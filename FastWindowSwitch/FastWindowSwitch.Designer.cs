﻿namespace FastWindowSwitch
{
    partial class FastWindowSwitch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboWindows = new global::FastWindowSwitch.AutocompleteCombo();
            this.SuspendLayout();
            // 
            // comboWindows
            // 
            this.comboWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboWindows.FormattingEnabled = true;
            this.comboWindows.Location = new System.Drawing.Point(12, 12);
            this.comboWindows.Name = "comboWindows";
            this.comboWindows.Size = new System.Drawing.Size(630, 21);
            this.comboWindows.TabIndex = 0;
            this.comboWindows.UnfilteredItems = new object[0];
            this.comboWindows.SelectionChangeCommitted += new System.EventHandler(this.comboWindows_SelectionChangeCommitted);
            this.comboWindows.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.comboWindows_PreviewKeyDown);
            // 
            // TabSwitcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 43);
            this.ControlBox = false;
            this.Controls.Add(this.comboWindows);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TabSwitcher";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FastWindowSwitch";
            this.ResumeLayout(false);

        }

        #endregion

        private AutocompleteCombo comboWindows;
    }
}
