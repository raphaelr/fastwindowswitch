FastWindowSwitch
================
**[Project homepage](https://tapesoftware.net/fastwindowswitch/)**

Ctrl-P like switchting between Windows applications.

# Usage

Press **Alt+PageUp** to open the window switcher. The desired window can be filtered both by the
title and the executable name. Press enter to confirm, or escape to cancel.

The program can be quit by right clicking on the new "FWS" icon in your taskbar notification area.
